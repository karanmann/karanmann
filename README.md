<div align="center">
<img src="https://rishavanand.github.io/static/images/greetings.gif" align="center" style="width: 100%" />
</div>  
  

# **<div align="center">Hi 👋, I'm Karan Mann</div>**  
  

### **<div align="center">A passionate frontend developer from Stockholm,Sweden</div>**  
  

#### <div align="center">Born and raised in India surrounded by MS-DOS and command lines 💻, living in Sweden. Somehow I diverged from my passion in coding. I'm a Frontend Developer in the making, computers were always a passion. Now I have jumped to the opportunity to working with my passion.</div>  
  

- 🌱 I’m currently learning TypeScript  
  

- 👨‍💻 All of my projects are available at https://karanmann.se/  
  

- 📝 I regularly write articles on https://medium.com/@karan-mann  
  

- 💬 Ask me about React, JavaScript, HTML, CSS, Sass, Node, Git, Linux, Phaser.js  
  

- 📫 How to reach me karan.mann@outlook.com  
  

- 📄 Know about my experiences https://www.linkedin.com/in/karanmann84/  
  

- ⚡ Fun fact I am a trained Chef from Le Cordon Bleu, London  
  

<br/>  

 ###  🫶 LETS CONNECT 
<div align="left">
<a href="https://github.com/karanmann" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://linkedin.com/in/karanmann84" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>
<a href="https://instagram.com/frontendchef" target="_blank">
<img src=https://img.shields.io/badge/instagram-%23000000.svg?&style=for-the-badge&logo=instagram&logoColor=white alt=instagram style="margin-bottom: 5px;" />
</a>
<a href="https://medium.com/@karan-mann" target="_blank">
<img src=https://img.shields.io/badge/medium-%23292929.svg?&style=for-the-badge&logo=medium&logoColor=white alt=medium style="margin-bottom: 5px;" />
</a>  
</div>  

<br />

 ### ⚡ TECHNOLOGIES
<table><tr><td valign="top" width="33%">



### Frontend  
<div align="center">  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/react-original-wordmark.svg" alt="React" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/bootstrap-plain.svg" alt="Bootstrap" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/css3-original-wordmark.svg" alt="CSS3" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/html5-original-wordmark.svg" alt="HTML5" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/javascript-original.svg" alt="JavaScript" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/typescript-original.svg" alt="TypeScript" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/git-scm-icon.svg" alt="Git" height="50" />  
</div>

</td><td valign="top" width="33%">



### Backend  
<div align="center">  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/mongodb-original-wordmark.svg" alt="MongoDB" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/nodejs-original-wordmark.svg" alt="Node.js" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/express-original-wordmark.svg" alt="Express.js" height="50" />  
</div>

</td><td valign="top" width="33%">



### DevOps  
<div align="center">  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/linux-original.svg" alt="Linux" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/gnu_bash-icon.svg" alt="Bash" height="50" />  
</div>

</td></tr></table>  

<br/>  

  <p align="center">
    <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=karanmann&show_icons=true&theme=gotham" alt="karanmann" />
  </p>
   <p align="center">
    <img align="center" src="https://github-readme-stats.vercel.app/api?username=karanmann&show_icons=true&theme=gotham" alt="karanmann" />
  </p>
  <p align="center">
    <img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=karanmann&theme=gotham" alt="karanmann" />
  </p>
  
  <br/>  

<div align="center">
<img src="https://komarev.com/ghpvc/?username=karanmann&&style=flat-square" align="center" />
</div>  
  

<br/>  

<!--**karanmann/karanmann** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.
